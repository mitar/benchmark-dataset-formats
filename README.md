## Goals

* Determine the best approach for long-term dataset storage.
* Have reasonable storage requirements for both text and binary data.
* It should be easy to transfer a dataset from a server.
* Measure time and memory consumption for writing and reading.
* Allows writing without having to keep all data in memory.
* Allows reading without having to keep all data in memory.
* Should be widely supported.

## Observations

SQLite:
* Built-in Python.
* Very easy to do, without having to read/learn a lot. Probably because of
  familiarity with tabular databases.
* Able to define constraints between tables.
* Metadata is not directly supported, but could be an additional standardized table,
  or some hacky way to insert into schema (SQLite preserves even comments used
  in `CREATE TABLE` queries).
* Storing binary data is stored as-is, without any noticeable overhead.
* I represented a bounding box in `PennFudanPed` dataset as JSON encoding in a column.
* Can be created incrementally, row by row, without having to load the whole table into memory.
* Supports columns of different types.
* It is one file.
* By default it supports only [2000 columns in a table](https://www.sqlite.org/limits.html).

HDF5:
* Had to spend more to figure how to use it.
* Support hierarchical organization, which means that we can express collections of datasets
  naturally, by simply creating a group of existing datasets and nest them. This can even
  work by referencing external HDF5 files, so that a collection of datasets can be very thin
  without any real data stored there.
* Supports [virtual datasets](http://docs.h5py.org/en/latest/vds.html): multiple datasets
  can be mapped together into a single (virtual) dataset (e.g., stacking datasets together
  along a new axis). Potentially useful to represent streaming datasets.
* Has support for custom metadata.
* Supports transparent compression.
* Supports compound data types so nested values can be represented.
* Supports references between tables. So one cell in a table can reference
  a subset of data in another table, but in practice it looks
  like a regular foreign key based approach is easier to work with when
  you are referencing just one row.
* It is one file.
* A complex standard.

Parquet:
* It is multiple files per each table. This means one has to provide information in some other way
  how those tables from a dataset.
* It does not support user-specified metadata. Metadata it supports seems to be about how chunks
  combine into a table.
* Could not find a Python library which would support writing out images one by one, but I would have
  to create one large in-memory DataFrame with all images and save that out.
* It seems support for [nested schemas are there](https://fastparquet.readthedocs.io/en/latest/details.html#reading-nested-schema)
  but I could not find a way to write them from Python.
* It seems [there is no way for now to define custom schemas from Python](https://stackoverflow.com/a/53730316/252025),
  so Python support is really just how to load and save Pandas DataFrames and nothing custom.
* It looks like it is really optimized for a particular way of processing data (through chunks of files)
  and it looks like more a format one prepares before data processing on a cluster and not how one stores
  raw data.
* I gave up.

Git:
* One can store data as-is, directly how it was obtained from a source in bunch of files, without
  having to do any data conversions.
* Native support for versioning of data.
* Native support for efficient syncing of changes between server and clients.
* Native detection of any data corruption (uses hashes internally for all data).
* Can be inspected with various tools (are just files).
* Does not require users to convert to one format (and no need for them to make decisions how to do this conversion).
* Uses a file system which can put load on the file system if there are many files.
* When writing or reading (but not during transfer or storage on the server) it requires double the disk space
  (one copy of data for its internal storage, one for checked out working copy).

## Measurements

Converting [Penn-Fudan Database for Pedestrian Detection and Segmentation](https://www.cis.upenn.edu/~jshi/ped_html/) dataset.

* Original ZIP file: 53,723,336 B
* SQLite: 54,087,680 B
* HDF5: 53,757,941 B
* git: 53,844,138 B

```
Elapsed time penn-fudan-ped-write-sqlite.py 0.4010267083067447 s
Elapsed time penn-fudan-ped-write-hdf5.py 0.6034669657004997 s
Elapsed time penn-fudan-ped-write-git.sh 1.706965293060057 s
CPU time penn-fudan-ped-write-sqlite.py 0.19412239999999997 s
CPU time penn-fudan-ped-write-hdf5.py 4.4382143 s
CPU time penn-fudan-ped-write-git.sh 2.2245117 s
Max memory penn-fudan-ped-write-sqlite.py 14935.6 KB
Max memory penn-fudan-ped-write-hdf5.py 57267.6 KB
Max memory penn-fudan-ped-write-git.sh 16575.6 KB
```

Converting [FMA](https://github.com/mdeff/fma) dataset.

* Original ZIP files: 100,664,524,632 B
* SQLite: 106,962,427,904 B
* HDF5: 106,419,793,925 B
* git: 106,064,883,973 B


```
Elapsed time fma-write-sqlite.py 340.78745313130787 s
Elapsed time fma-write-hdf5.py 310.6563945960021 s
Elapsed time fma-write-git.sh 11578.14116650084 s
CPU time fma-write-sqlite.py 197.72261440000003 s
CPU time fma-write-hdf5.py 244.83206230000002 s
CPU time fma-write-git.sh 15186.102816499999 s
Max memory fma-write-sqlite.py 5015570.4 KB
Max memory fma-write-hdf5.py 9045633.6 KB
Max memory fma-write-git.sh 201297.2 KB
```

## Running

Use Python 3.6 or newer. Install Python dependencies:

```
$ pip3 install -r requirements.txt
```

You should also have git and git LFS installed:

```
$ apt-get install git git-lfs
```
