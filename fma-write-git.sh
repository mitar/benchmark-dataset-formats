#!/bin/bash -e

# A script which converts FMA to git.
#
# Prepare data:
#   wget https://os.unil.cloud.switch.ch/fma/fma_metadata.zip
#   wget https://os.unil.cloud.switch.ch/fma/fma_large.zip
#   unzip fma_metadata.zip
#   unzip fma_large.zip
#
# Output is "fma" directory.

INPUT_METADATA_DIRECTORY="fma_metadata"
INPUT_DATA_DIRECTORY="fma_large"
OUTPUT_DIRECTORY="fma"

mkdir "$OUTPUT_DIRECTORY"
mv "$INPUT_METADATA_DIRECTORY" "fma"
mv "$INPUT_DATA_DIRECTORY" "fma"

git -C "$OUTPUT_DIRECTORY" init
git -C "$OUTPUT_DIRECTORY" lfs install

# Put all files larger than 100 KB into git LFS.
(cd "$OUTPUT_DIRECTORY"; find . -type f -size +100k -exec git lfs track '{}' +)

git -C "$OUTPUT_DIRECTORY" add .gitattributes
git -C "$OUTPUT_DIRECTORY" add .
GIT_COMMITTER_NAME='Test' GIT_COMMITTER_EMAIL='test@example.com' git -C "$OUTPUT_DIRECTORY" commit --author='Test <test@example.com>' -m 'Initial version of the dataset.'
