#!/usr/bin/env python3

# A script which runs measurements for writing FMA to different formats.

import os
import os.path
import subprocess
import sys
import time

METADATA_URL = 'https://os.unil.cloud.switch.ch/fma/fma_metadata.zip'
DATA_URL = 'https://os.unil.cloud.switch.ch/fma/fma_large.zip'
EXTRACTED_METADATA_DIR = 'fma_metadata'
EXTRACTED_DATA_DIR = 'fma_large'
SCRIPTS = ['fma-write-sqlite.py', 'fma-write-hdf5.py', 'fma-write-git.sh']
OUTPUT_FILES = ['fma.sqlite', 'fma.hdf5', 'fma']


def directory_size(directory_path):
    size = 0

    for dirpath, dirnames, filenames in os.walk(directory_path):
        for filename in filenames:
            file_path = os.path.join(dirpath, filename)
            if not os.path.islink(file_path):
                size += os.path.getsize(file_path)

    return size


def main():
    subprocess.run(['wget', '--no-clobber', METADATA_URL], check=True)
    subprocess.run(['wget', '--no-clobber', DATA_URL], check=True)

    print("Main prepare")

    if not os.path.exists(EXTRACTED_METADATA_DIR):
        subprocess.run(['unzip', '-q', os.path.basename(METADATA_URL)], check=True)
    if not os.path.exists(EXTRACTED_DATA_DIR):
        subprocess.run(['unzip', '-q', os.path.basename(DATA_URL)], check=True)

    elapsed_time_measurements = {}
    cpu_time_measurements = {}
    memory_measurements = {}

    for script, output_file in zip(SCRIPTS, OUTPUT_FILES):
        elapsed_time_measurements[script] = []
        cpu_time_measurements[script] = []
        memory_measurements[script] = []

        for i in range(10):
            print("Prepare", i, script)

            if os.path.exists(f'fma/{EXTRACTED_METADATA_DIR}'):
                os.rename(f'fma/{EXTRACTED_METADATA_DIR}', EXTRACTED_METADATA_DIR)
            if os.path.exists(f'fma/{EXTRACTED_DATA_DIR}'):
                os.rename(f'fma/{EXTRACTED_DATA_DIR}', EXTRACTED_DATA_DIR)

            subprocess.run(['rm', '-rf', output_file], check=True)

            print("Run")

            before = time.perf_counter()
            with subprocess.Popen([f'./{script}'], stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=sys.stderr) as process:
                # This waits for the main subprocess to finish and then wait4 uses internally RUSAGE_BOTH to get
                # resources of the main subprocess and any children that subprocess might have made.
                child_pid, exit_code, rusage = os.wait4(process.pid, 0)
                after = time.perf_counter()

            if exit_code != 0:
                print("Failed.")
                sys.exit(1)

            elapsed_time_measurements[script].append(after - before)
            # We sum user and kernel time.
            cpu_time_measurements[script].append(rusage.ru_utime + rusage.ru_stime)
            memory_measurements[script].append(rusage.ru_maxrss)

            if 'git' in script:
                print("Size", directory_size(f'{output_file}/.git'), "B")
            else:
                print("Size", os.path.getsize(output_file), "B")

    for key, values in elapsed_time_measurements.items():
        print("Elapsed time", key, sum(values) / len(values), "s")

    for key, values in cpu_time_measurements.items():
        print("CPU time", key, sum(values) / len(values), "s")

    for key, values in memory_measurements.items():
        print("Max memory", key, sum(values) / len(values), "KB")


if __name__ == '__main__':
    main()
