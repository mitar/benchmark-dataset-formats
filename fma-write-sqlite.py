#!/usr/bin/env python3

# A script which converts FMA to SQLite.
#
# Prepare data:
#   wget https://os.unil.cloud.switch.ch/fma/fma_metadata.zip
#   wget https://os.unil.cloud.switch.ch/fma/fma_large.zip
#   unzip fma_metadata.zip
#   unzip fma_large.zip
#
# Output is "fma.sqlite" file.

import glob
import os
import os.path
import sqlite3

import pandas

INPUT_METADATA_DIRECTORY="fma_metadata"
INPUT_DATA_DIRECTORY="fma_large"
OUTPUT_FILE = 'fma.sqlite'


def files():
    for filename in glob.glob(f'{INPUT_DATA_DIRECTORY}/*/*.mp3'):
        with open(filename, 'rb') as file:
            yield (os.path.basename(filename), file.read())


def main():
    for input_directory in [INPUT_METADATA_DIRECTORY, INPUT_DATA_DIRECTORY]:
        if not os.path.exists(input_directory):
            raise RuntimeError(f"Missing '{input_directory}' directory.")

    for filename in [OUTPUT_FILE, f'{OUTPUT_FILE}-wal']:
        if os.path.exists(filename):
            os.remove(filename)

    connection = sqlite3.connect(OUTPUT_FILE)

    for filename in glob.glob(f'{INPUT_METADATA_DIRECTORY}/*.csv'):
        dataframe = pandas.pandas.read_csv(
            filename,
            # We do not want to do any conversion of values.
            dtype=str,
            # We always expect one row header.
            header=0,
            # We want empty strings and not NaNs.
            na_filter=False,
            encoding='utf8',
        )
        dataframe.to_sql(os.path.splitext(os.path.basename(filename))[0], con=connection, if_exists='fail', index=False)

    cursor = connection.cursor()
    cursor.execute("CREATE TABLE files(filename TEXT PRIMARY KEY, audio BLOB)")

    cursor.executemany("INSERT INTO files(filename, audio) VALUES (?, ?)", files())

    connection.commit()
    connection.close()


if __name__ == '__main__':
    main()
