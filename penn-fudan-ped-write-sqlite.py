#!/usr/bin/env python3

# A script which converts Penn-Fudan Database for Pedestrian Detection and Segmentation to SQLite.
#
# Prepare data:
#   wget https://www.cis.upenn.edu/~jshi/ped_html/PennFudanPed.zip
#   unzip PennFudanPed.zip
#
# Output is "penn-fudan.sqlite" file.

import json
import glob
import os
import os.path
import re
import sqlite3

INPUT_DIRECTORY = 'PennFudanPed'
OUTPUT_FILE = 'penn-fudan.sqlite'
NAME_REGEX = re.compile(r'"([^"]*?)"')
NUMBER_REGEX = re.compile(r'\d+')


def images(directory):
    for filename in glob.glob(f'{INPUT_DIRECTORY}/{directory}/*.png'):
        with open(filename, 'rb') as file:
            yield (os.path.basename(filename), file.read())


def unquote(name):
    return NAME_REGEX.match(name)[1]


def parse_annotation(filename):
    annotation = {}
    with open(filename, 'r', encoding='utf8') as file:
        for line in file.read().splitlines():
            if line.startswith('#'):
                continue

            segments = line.split(' : ', 1)
            if len(segments) != 2:
                continue

            annotation[segments[0]] = segments[1]

    image_filename = os.path.basename(unquote(annotation['Image filename']))
    object_names = NAME_REGEX.findall(annotation['Objects with ground truth'])

    for i, object_name in enumerate(object_names, start=1):
        bounding_box = annotation[f'Bounding box for object {i} "{object_name}" (Xmin, Ymin) - (Xmax, Ymax)']
        mask_filename = os.path.basename(unquote(annotation[f'Pixel mask for object {i} "{object_name}"']))

        yield (image_filename, mask_filename, json.dumps(dict(zip(('Xmin', 'Ymin', 'Xmax', 'Ymax'), map(int, NUMBER_REGEX.findall(bounding_box)))), ensure_ascii=False, allow_nan=False, separators=(',', ':')))


def parse_annotations():
    for i, filename in enumerate(glob.glob(f'{INPUT_DIRECTORY}/Annotation/*.txt')):
        for annotation in parse_annotation(filename):
            yield (i,) + annotation


def main():
    if not os.path.exists(INPUT_DIRECTORY):
        raise RuntimeError(f"Missing '{INPUT_DIRECTORY}' directory.")

    for filename in [OUTPUT_FILE, f'{OUTPUT_FILE}-wal']:
        if os.path.exists(filename):
            os.remove(filename)

    connection = sqlite3.connect(OUTPUT_FILE)
    cursor = connection.cursor()

    cursor.execute("CREATE TABLE PNGImages(filename TEXT PRIMARY KEY, image BLOB)")
    cursor.execute("CREATE TABLE PedMasks(filename TEXT PRIMARY KEY, image BLOB)")
    cursor.execute("CREATE TABLE Annotations(sample_index INTEGER, image_filename TEXT, mask_filename TEXT, bounding_box TEXT, FOREIGN KEY(image_filename) REFERENCES PNGImages(filename), FOREIGN KEY(mask_filename) REFERENCES PedMasks(filename))")

    cursor.executemany("INSERT INTO PNGImages(filename, image) VALUES (?, ?)", images('PNGImages'))
    cursor.executemany("INSERT INTO PedMasks(filename, image) VALUES (?, ?)", images('PedMasks'))
    cursor.executemany("INSERT INTO Annotations(sample_index, image_filename, mask_filename, bounding_box) VALUES (?, ?, ?, ?)", parse_annotations())

    connection.commit()
    connection.close()


if __name__ == '__main__':
    main()
