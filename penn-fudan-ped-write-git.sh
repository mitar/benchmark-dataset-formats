#!/bin/bash -e

# A script which converts Penn-Fudan Database for Pedestrian Detection and Segmentation to git.
#
# Prepare data:
#   wget https://www.cis.upenn.edu/~jshi/ped_html/PennFudanPed.zip
#   unzip PennFudanPed.zip
#
# Output is converted "PennFudanPed" directory itself.

INPUT_DIRECTORY="PennFudanPed"

git -C "$INPUT_DIRECTORY" init
git -C "$INPUT_DIRECTORY" lfs install

# Put all files larger than 100 KB into git LFS.
(cd "$INPUT_DIRECTORY"; find . -type f -size +100k -exec git lfs track '{}' +)

git -C "$INPUT_DIRECTORY" add .gitattributes
git -C "$INPUT_DIRECTORY" add .
GIT_COMMITTER_NAME='Test' GIT_COMMITTER_EMAIL='test@example.com' git -C "$INPUT_DIRECTORY" commit --author='Test <test@example.com>' -m 'Initial version of the dataset.'
