#!/usr/bin/env python3

# A script which converts FMA to HDF5.
#
# Prepare data:
#   wget https://os.unil.cloud.switch.ch/fma/fma_metadata.zip
#   wget https://os.unil.cloud.switch.ch/fma/fma_large.zip
#   unzip fma_metadata.zip
#   unzip fma_large.zip
#
# Output is "fma.hdf5" file.

import glob
import os
import os.path

import h5py
import numpy as np
import pandas

INPUT_METADATA_DIRECTORY="fma_metadata"
INPUT_DATA_DIRECTORY="fma_large"
OUTPUT_FILE = 'fma.hdf5'


def files():
    for filename in glob.glob(f'{INPUT_DATA_DIRECTORY}/*/*.mp3'):
        with open(filename, 'rb') as file:
            yield (os.path.basename(filename), np.fromfile(file, dtype='uint8'))


def main():
    for input_directory in [INPUT_METADATA_DIRECTORY, INPUT_DATA_DIRECTORY]:
        if not os.path.exists(input_directory):
            raise RuntimeError(f"Missing '{input_directory}' directory.")

    with pandas.HDFStore(OUTPUT_FILE, 'w', libver='latest') as store:
        for filename in glob.glob(f'{INPUT_METADATA_DIRECTORY}/*.csv'):
            dataframe = pandas.pandas.read_csv(
                filename,
                # We do not want to do any conversion of values.
                dtype=str,
                # We always expect one row header.
                header=0,
                # We want empty strings and not NaNs.
                na_filter=False,
                encoding='utf8',
            )
            store[os.path.splitext(os.path.basename(filename))[0]] = dataframe

    file = h5py.File(OUTPUT_FILE, 'r+', libver='latest')

    files_dtype = np.dtype([
        ('filename', h5py.string_dtype(encoding='utf-8')),
        ('audio', h5py.vlen_dtype(np.uint8)),
    ])
    files_dataset = file.create_dataset('files', shape=(0,), maxshape=(None,), dtype=files_dtype, compression='gzip', compression_opts=9)

    for row in files():
        files_dataset.resize(files_dataset.shape[0] + 1, axis=0)
        files_dataset[-1] = row

    file.flush()
    file.close()


if __name__ == '__main__':
    main()
