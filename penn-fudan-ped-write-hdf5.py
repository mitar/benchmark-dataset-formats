#!/usr/bin/env python3

# A script which converts Penn-Fudan Database for Pedestrian Detection and Segmentation to HDF5.
#
# Prepare data:
#   wget https://www.cis.upenn.edu/~jshi/ped_html/PennFudanPed.zip
#   unzip PennFudanPed.zip
#
# Output is "penn-fudan.hdf5" file.

import glob
import os
import os.path
import re

import h5py
import numpy as np

INPUT_DIRECTORY = 'PennFudanPed'
OUTPUT_FILE = 'penn-fudan.hdf5'
NAME_REGEX = re.compile(r'"([^"]*?)"')
NUMBER_REGEX = re.compile(r'\d+')


def images(directory):
    for filename in glob.glob(f'{INPUT_DIRECTORY}/{directory}/*.png'):
        with open(filename, 'rb') as file:
            yield (os.path.basename(filename), np.fromfile(file, dtype='uint8'))


def unquote(name):
    return NAME_REGEX.match(name)[1]


def parse_annotation(filename):
    annotation = {}
    with open(filename, 'r', encoding='utf8') as file:
        for line in file.read().splitlines():
            if line.startswith('#'):
                continue

            segments = line.split(' : ', 1)
            if len(segments) != 2:
                continue

            annotation[segments[0]] = segments[1]

    image_filename = os.path.basename(unquote(annotation['Image filename']))
    object_names = NAME_REGEX.findall(annotation['Objects with ground truth'])

    for i, object_name in enumerate(object_names, start=1):
        bounding_box = annotation[f'Bounding box for object {i} "{object_name}" (Xmin, Ymin) - (Xmax, Ymax)']
        mask_filename = os.path.basename(unquote(annotation[f'Pixel mask for object {i} "{object_name}"']))

        yield (image_filename, mask_filename, tuple(NUMBER_REGEX.findall(bounding_box)))


def parse_annotations():
    for i, filename in enumerate(glob.glob(f'{INPUT_DIRECTORY}/Annotation/*.txt')):
        for annotation in parse_annotation(filename):
            yield (i,) + annotation


def main():
    if not os.path.exists(INPUT_DIRECTORY):
        raise RuntimeError(f"Missing '{INPUT_DIRECTORY}' directory.")

    file = h5py.File(OUTPUT_FILE, 'w', libver='latest')

    png_images_dtype = np.dtype([
        ('filename', h5py.string_dtype(encoding='utf-8')),
        ('sequence', h5py.vlen_dtype(np.uint8)),
    ])
    png_images = file.create_dataset('PNGImages', shape=(0,), maxshape=(None,), dtype=png_images_dtype, compression='gzip', compression_opts=9)
    png_images_map = {}

    for row in images('PNGImages'):
        png_images_map[row[0]] = png_images.shape[0]
        png_images.resize(png_images.shape[0] + 1, axis=0)
        png_images[-1] = row

    ped_masks_dtype = np.dtype([
        ('filename', h5py.string_dtype(encoding='utf-8')),
        ('sequence', h5py.vlen_dtype(np.uint8)),
    ])
    ped_masks = file.create_dataset('PedMasks', shape=(0,), maxshape=(None,), dtype=ped_masks_dtype, compression='gzip', compression_opts=9)
    ped_masks_map = {}

    for row in images('PedMasks'):
        ped_masks_map[row[0]] = ped_masks.shape[0]
        ped_masks.resize(ped_masks.shape[0] + 1, axis=0)
        ped_masks[-1] = row

    annotations_dtype = np.dtype([
        ('sample_index', np.uint32),
        ('image_filename', h5py.string_dtype(encoding='utf-8')),
        ('mask_filename', h5py.string_dtype(encoding='utf-8')),
        ('bounding_box', np.dtype([
            ('Xmin', np.uint32),
            ('Ymin', np.uint32),
            ('Xmax', np.uint32),
            ('Ymax', np.uint32),
        ])),
    ])
    annotations = file.create_dataset('Annotations', shape=(0,), maxshape=(None,), dtype=annotations_dtype, compression='gzip', compression_opts=9)

    for row in parse_annotations():
        annotations.resize(annotations.shape[0] + 1, axis=0)
        annotations[-1] = row

    file.flush()
    file.close()


if __name__ == '__main__':
    main()
